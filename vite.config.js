import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 5000, // Change this to the desired port (e.g., 5000)
  },
  plugins: [svelte()],
})
