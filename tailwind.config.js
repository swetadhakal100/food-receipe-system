/** @type {import('tailwindcss').Config} */
export default {
  content: [ "./index.html",
  "./src/**/*.{svelte,js,ts}",],
  theme: {  
    extend: {
      color: {
        'green': '#129575',
          '50': '#f0fdf4',
          '100': '#dcfce7',
          '900': '#22543d',
      },
      fontFamily: {
        poppins: ['Poppins'],
      },
      height:{
         '30': '7.5rem',
      },
      width:{
        '53': '13.1rem',
      },
      margin:{
        '39': "9rem",
      },
      fontSize: {
        'xxs': '0.5rem', 
        '3xl': '1.953rem',
      },
      padding:{
        '98': '32rem',
      },
      backgroundImage: {
        'main-body': "url('./src/assets/image/screenimg.jpg')",
      },
      
     
    

    
     
    },
  },
  plugins: [],
}

